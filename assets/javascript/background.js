var requestOptions = {
    method: 'GET',
    redirect: 'follow',
};

fetch("https://9b3e119vh8.execute-api.eu-west-2.amazonaws.com/Prod/web/background", requestOptions)
    .then(response => response.text())
    .then(result => {
        var background = document.getElementById('background');
        background.style.backgroundImage = "url('" + result + "')"
        background.style.backgroundRepeat = 'no-repeat'
        background.style.backgroundSize = "auto"
        background.style.backgroundColor = "black"
        background.style.backgroundPosition = "center center"
        background.style.backgroundSize = "cover"
        document.body.style.backgroundColor = "black"

        var containerElement = document.getElementById('main_container');
        var overlayEle = document.getElementById('overlay');

        if (state) {
            overlayEle.style.display = 'block';
            containerElement.setAttribute('class', 'blur');
        } else {
            overlayEle.style.display = 'none';
            containerElement.setAttribute('class', null);
        }
    })
    .catch(error => console.log('error', error));